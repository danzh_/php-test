<?php
    session_start();
    if (!isset($_SESSION['SESSION_EMAIL'])) {
        header("Location: index.php");
    }

    include 'config.php';

    $sql = mysqli_query($conn, "SELECT * FROM users WHERE email='{$_SESSION['SESSION_EMAIL']}'");
    
    if (mysqli_num_rows($sql) > 0) {
        $row = mysqli_fetch_assoc($sql);

        // echo $row["email"];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap-5.2.0-dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css">
    <title>Welcome</title>
</head>
<body>

<header>

    <nav class="navbar navbar-expand-lg fixed-top navbar-brand-center" role="navigation">
    <div class="collapse navbar-collapse order-lg-1 order-3" id="navbarNav">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link aa" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link aa" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link aa" href="logout.php">Logout</a>
                </li>
            </ul>
        </div>


        <a class="navbar-brand order-1" href="#">
            <img class="logo-white" style="width: 100%; height: auto;" src="img/logo-1.svg" alt="logo">
        </a>

        <button class="navbar-toggler order-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-lg-3 order-4" id="navbarNav">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link aa" href="#">Service</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link aa" href="#">Contact</a>
                </li>
            </ul>
        </div>
    </nav>
    
    <section class="page-header">
        
        <div class="container justify-content-center text-center">
            <div class="row">
                <div class="col-12 pb-3 pt-5">
                    I am Open Sans 120px
                </div>
            </div>
        </div>
        
    </section>

</header>
    
<section class="middle-section mt-5 justify-content-center text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 h2 font-weight-bold">I am open sans extra bold 48px</div>
            <div class="col-12 font-weight-light">Please follow all directions, make fonts the same size, respect margins and spacing</div>
        </div>
    </div>
</section>

<section class="images-section m-4">
    <div class="container">
        <div class="row">
            <div class="col-md-4"><img class="img-fluid m-2" src="img/square-img-1.jpg" alt="square image"></div>
            <div class="col-md-4"><img class="img-fluid m-2" src="img/square-img-2.jpg" alt="square image"></div>
            <div class="col-md-4"><img class="img-fluid m-2" src="img/square-img-3.jpg" alt="square image"></div>
        </div>
    </div>
</section>

<script src="bootstrap-5.2.0-dist/js/bootstrap.min.js"></script>
</body>
</html>