<?php
    session_start();
    include "config.php";
    $msg = "";

    if (isset($_GET['verification'])) {
        if (mysqli_num_rows(mysqli_query($conn, "SELECT * FROM users WHERE code='{$_GET['verification']}'")) > 0) {
            $query = mysqli_query($conn, "UPDATE users SET code='' WHERE code='{$_GET['verification']}'");

            if ($query) {
                $msg = "<div class='alert alert-success'>Your email has been successfully verified!</div>";
            }
        }
        else {
            header("Location:index.php");
        }
    }

    if (isset($_POST["submit"])) {
        $email = mysqli_real_escape_string($conn, $_POST["email"]);
        $password = mysqli_real_escape_string($conn, md5($_POST["password"]));

        $sql = "SELECT * FROM users WHERE email='{$email}' AND password='{$password}'";
        $result = mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) === 1) {
            $row = mysqli_fetch_assoc($result);

            if (!empty($row['code'])) {
                $msg = "<div class='alert alert-danger'>Account not verified!</div>";
            }
            else {
                $_SESSION['SESSION_EMAIL'] = $email;
                header("Location:welcome.php");
                $msg = "<div class='alert alert-info'>Success!</div>";
            }
        }
        else {
            $msg = "<div class='alert alert-danger'>Wrong credentials</div>";
        }
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  </head>
  <body>

    <div class="text-center">
        <form style="max-width:360px; margin:auto" method="POST">
            <h1 class="h3 font-weight-normal mb-3 mt-4">Please Sing In</h1>

            <?php echo $msg; ?>

            <input name="email" type="email" class="form-control mb-2" 
            id="emailAddress" placeholder="Email Address" required autofocus>

            <input name="password" type="password" class="form-control mt-2" 
            id="password" placeholder="Password" required>
            
            <div class="d-flex justify-content-between mt-3">
                <div>
                    <button name="submit" class="btn btn-primary">Sing In</button>
                </div>
                <div>
                    <a href="register.php">Sing Up</a>
                </div>
            </div>

        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

  </body>
</html>