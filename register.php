<?php 

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    use PHPMailer\PHPMailer\SMTP;

    require 'vendor/autoload.php';

    include "config.php";
    $msg = "";

    if(isset($_POST["submit"])) {

        $name = mysqli_real_escape_string($conn, $_POST["name"]);
        $email = mysqli_real_escape_string($conn, $_POST["email"]);
        $password = mysqli_real_escape_string($conn, md5($_POST["password"]));
        $confirm_password = mysqli_real_escape_string($conn, md5($_POST["confirm-password"]));
        $code = mysqli_real_escape_string($conn, md5(rand()));

        if (mysqli_num_rows(mysqli_query($conn, "SELECT * FROM users WHERE email='{$email}'")) > 0) {
            $msg = "<div class='alert alert-danger'>{$email} - This email already exists</div>";
        }
        else {
            if ($password === $confirm_password) {
                $sql = "INSERT INTO users (`name`, `email`, `password`, `code`) 
                VALUES ('{$name}', '{$email}', '{$password}', '{$code}')";


                $result = mysqli_query($conn, $sql);

                if ( false===$result ) {
                    printf("error: %s\n", mysqli_error($conn));
                }

                // echo $result;
                
                if ($result) {
                    echo "<div style='display:none;'>";

                    $mail = new PHPMailer(true);

                    try {
                        //Server settings
                        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                        $mail->isSMTP();                                            //Send using SMTP
                        $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                        $mail->Username   = 'phptestmail223@gmail.com';                     //SMTP username
                        $mail->Password   = 'aaohfxdqufidbdhc';                               //SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                        $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                    
                        //Recipients
                        $mail->setFrom('phptestmail223@gmail.com');
                        $mail->addAddress($email);
                    
                        //Content
                        $mail->isHTML(true);                                  //Set email format to HTML
                        $mail->Subject = 'no reply';
                        $mail->Body    = 'Here is your verification link <b><a href="http://localhost/test/?verification='.$code.'">http://localhost/test/?verification='.$code.'</a></b>';
                    
                        $mail->send();
                        echo 'Message has been sent';
                    } catch (Exception $e) {
                        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                    }

                    echo "</div>";

                    $msg = "<div class='alert alert-success'>Verification link has 
                    been sent on your email</div>";
                }
                else {
                    $msg = "<div class='alert alert-danger'>Something went wrong, sorry!</div>";
                }
            }
            else {
                $msg = "<div class='alert alert-danger'>Password and 
                Confirm Password do not match</div>";
            }
        }
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  </head>
  <body>

    <div class="text-center">
        <?php echo $msg; ?>
        <form style="max-width:360px; margin:auto" method="POST" action="">
            <h1 class="h3 font-weight-normal mb-3 mt-4">Register Now</h1>

            <input type="text" id="name" placeholder="Name" 
            class="form-control mb-2" name="name" value="<?php if (isset($_POST['submit'])) { echo $name; } ?>" required autofocus>

            <input type="email" class="form-control mb-2 mt-2" name="email"
            id="emailAddress" placeholder="Email Address" value="<?php if (isset($_POST['submit'])) { echo $email; } ?>" required>

            <input type="password" class="form-control mt-2" name="password"
            id="password" placeholder="Password" required>

            <input type="password" class="form-control mt-2" name="confirm-password"
            id="confirmPassword" placeholder="Confirm Password" required>
            
            <div class="d-flex justify-content-between mt-3">
                <div>
                    <button name="submit" class="btn btn-primary">Sing Up</button>
                </div>
                <div>
                    <a href="index.php">Sing In</a>
                </div>
            </div>

        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

  </body>
</html>